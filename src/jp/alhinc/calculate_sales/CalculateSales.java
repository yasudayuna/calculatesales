package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String OVER_10DIGIT = "合計金額が10桁を超えました";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイルが連番になっていません";
	private static final String FILE_INVALID_BRANCHCODE = "該当ファイル名(00000001.rcdなど)の支店コードが不正です";
	private static final String SALESFILE_INVALID_FORMAT = "該当ファイル名(00000001.rcdなど)のフォーマットが不正です";


	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)

			File[] files = new File(args[0]).listFiles();
			List<File> rcdFiles = new ArrayList<>();

			for(int i = 0; i < files.length ; i++) {
				String fileNames = files[i].getName();
				if (fileNames.matches("^[0-9] {8} .rcd$" )) {
							rcdFiles.add(files[i]);

							long fileSales = Long.parseLong(fileNames);
							Long saleAmount = branchSales.get(fileNames) + fileSales;
							if(saleAmount >= 10000000000L) {
								System.out.println("合計金額が10桁を超えました");
								saleAmount.close();
								return;
							}


			}




		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			 System.out.println("売上ファイル名が連番になっています");
	      } else {
	        System.out.println("売上ファイル名が連番になっていません");
	      }
	    }

			return;
		}
		}



	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {

				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");
//				System.out.println(line);

				branchNames.put(items[0],items[1]);
				branchSales.put(items[0], 0L);


			}


		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);

			if (!file.exists()) { /*エラー1-1 ファイルの存在確認*/
				System.out.println(fw + FILE_NOT_EXIST);
				return false;
			} else if (!file.isFile()) { /*エラーその他 ファイルかどうか*/
				System.out.println(UNKNOWN_ERROR);
				return false;
			} else {
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
			}

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				String[] items = line.split(",");
				/*エラー1-2 読み込みファイルのフォーマット確認*/

			} else if (!file.isFile()) { /*エラーその他 ファイルかどうか*/
				System.out.println(UNKNOWN_ERROR);
				return false;
			} else {
			bw = new BufferedWriter(fw);
			for (String key:branchNames.keySet()) {
			String a =( branchNames.get(key) + "," + branchSales.get(key) + "," + key);


				bw.write(a);
//
//				Long b = (int)branchSales.get(key);
//				bw.write(b);

				bw.newLine();

			}





				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;

				} finally {
					// ファイルを開いている場合

					if(bw != null) {
						try {
							// ファイルを閉じる
							bw.close();
						} catch(IOException e){
							System.out.println(UNKNOWN_ERROR);
							return false;
						}
					}
				}



		return true;
	}

}
